class ConvertToUtf4mb4 < ActiveRecord::Migration[5.0]
  # List of  the tables that we don't do the convert
  EXCLUDE_TABLES = { 'ar_internal_metadata' => true }.freeze

  # Default text-type length of MySQL
  TINY_TEXT_LIMIT = 65535

  OLD_DB_SETTINGS = {
    encoding:   'latin1',
    collation:  'latin1_swedish_ci',
    row_format: 'COMPACT'
  }.freeze

  NEW_DB_SETTINGS = {
    encoding:   'utf8mb4',
    collation:  'utf8mb4_unicode_ci',
    row_format: 'DYNAMIC'
  }.freeze

  def up
    perform_convert(NEW_DB_SETTINGS)
  end

  def down
    perform_convert(OLD_DB_SETTINGS)
  end

  # Main method that converts database, database tables and columns to old or new db settings
  # @param  db_settings [Hash] a hash that contains encoding, collation and row_format to be set
  # @option db_settings [String] :encoding   character set of database, tables and columns
  # @option db_settings [String] :collation  collation of database, tables and columns
  # @option db_settings [String] :row_format row_format of tables. For utfmb4, set to DYNAMIC. For old encoding, set to COMPACT
  def perform_convert(db_settings)
    return unless connection.adapter_name.downcase == 'mysql2'

    encoding   = db_settings[:encoding]
    collation  = db_settings[:collation]
    row_format = db_settings[:row_format]

    ActiveRecord::Base.transaction do
      # Alter the database to specific encoding
      database_name = ActiveRecord::Base.connection_config[:database]
      puts "\n\nWorking on database #{database_name} ..."
      connection.execute("ALTER DATABASE #{database_name} CHARACTER SET = #{encoding} COLLATE = #{collation}")
      connection.execute('commit')

      # Alter table's row format and character set
      tables = connection.tables
      tables.each do |table_name|
        next if EXCLUDE_TABLES[table_name]
        puts "\nWorking on table #{table_name} ..."
        connection.execute("ALTER TABLE #{table_name} ROW_FORMAT=#{row_format}")
        connection.execute("ALTER TABLE #{table_name} CONVERT TO CHARACTER SET #{encoding} COLLATE  #{collation}")

        # Alter all the string columns of the table to specific encoding and collation
        columns = connection.columns(table_name).select { |x| x.sql_type.match(/(char|text)/) }
        columns.each do |column|
          puts "- Working on column: #{table_name}.#{column.name} ..."
          sql_type = (column.sql_type == 'text' && column.limit == TINY_TEXT_LIMIT) ? 'mediumtext' : column.sql_type
          sql_statement = "ALTER TABLE #{table_name} CHANGE #{table_name}.#{column.name} #{table_name}.#{column.name} #{sql_type} CHARACTER SET #{encoding} COLLATE  #{collation}"
          puts(sql_statement)
          connection.execute(sql_statement)
          puts("Finish #{sql_statement}")
        end

        puts "\nRepairing and optimizing table #{table_name} ..."
        connection.execute("REPAIR TABLE #{table_name}")
        connection.execute("OPTIMIZE TABLE #{table_name}")
      end

      connection.execute('commit')
    end #transaction
  end
  
  # Convenient method that return a database connection
  # to avoid writing 'ActiveRecord::Base.connection' all over the places
  # @return [ActiveRecord::ConnectionAdapters::Mysql2Adapter] A database adapter, in MySQL case: MySQL adapter
  def connection
    @@connection ||= ActiveRecord::Base.connection
  end
end
