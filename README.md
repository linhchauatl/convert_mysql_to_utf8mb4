### Automate the converting a MySQL database's character set to utfmb4


Please refer to this article: [Automate the converting a MySQL database character set to utf8mb4](http://hanoian.com/content/index.php/24-automate-the-converting-a-mysql-database-character-set-to-utf8mb4)

The conversion script is written as an ActiveRecord migration script that automates all the manual conversion steps. There is also a test script that can be run with `rails runner` to verify the correctness of the conversion. Of course you can modify them into Ruby programs that can be called in stand-alone Ruby environment without using Rails.