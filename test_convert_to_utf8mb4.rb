# NOTICE: This test script writes data to database. DO NOT USE IT IN PRODUCTION.

# Usage:
#  From terminal command prompt in Rails.root, run: rails runner script/test_convert_to_utf8mb4.rb
# or
#  Within Rails console, run: puts `rails runner script/test_convert_to_utf8mb4.rb`

require "#{Rails.root}/spec/rails_helper"

TINY_TEXT_LIMIT = 65535
MAX_UTF8_MB4_BYTESIZE = 4

# This is the method performs the test of validating and writing all the text data to all tables/columns
# @param string_to_write [String] string to test, can be any string that has utfmb4 characters
# @raise [Mysql2::Error, ActiveRecord::RecordInvalid, ActiveModel::ValidationError] if the data causes error
# @return [void] if all the tests passed
def perform_test(string_to_write)
  puts "\n\nTest writing data: \"#{string_to_write}\""

  connection = ActiveRecord::Base.connection
  ar_classes = ActiveRecord::Base.descendants

  ActiveRecord::Base.transaction do
    # Test all ActiveRecord types in the system
    ar_classes.each do |klass|
      puts "Test class #{klass.name}"
      table_name = klass.table_name
      next unless table_name
      puts "Test table #{table_name}"
      obj = retrieve_object(klass)
      next unless obj
      puts "Test object #{obj}"
      # Test char/text columns
      columns = connection.columns(table_name)
      columns.each do |column|
        next unless column.sql_type.downcase.match(/(char|text)/)
        # Save origin_value
        origin_value = obj.send(column.name)

        validators = klass.validators_on(column.name).select { |x| x.class.name.match(/Validations::LengthValidator/) }
        validate_obj(obj, validators, string_to_write) if validators.any?

        begin
          puts "Test writing to column `#{column.name}` of `#{table_name}` ... "
          limit = (column.limit > TINY_TEXT_LIMIT) ? TINY_TEXT_LIMIT * MAX_UTF8_MB4_BYTESIZE : column.limit
          written_value = (string_to_write * limit)[0...limit]
          puts "Value's size = #{written_value.size} -- bytes: #{written_value.bytesize}"
          # By pass other validations, just run the update to see if it works
          connection.execute("UPDATE #{table_name} SET #{table_name}.#{column.name} = '#{written_value}' WHERE #{table_name}.id = #{obj.id}")
        ensure
          # Restore the origin_value
          obj.send("#{column.name}=", origin_value)
          obj.save!
        end
      end # loop through columns
    end # loop through all ActiveRecord types
  end # transaction
end

# This method returns an object to test
# @param class_to_retrieve [Class] class of the object to retrieve
# @return [ActiveRecord::Base] an object with the class passed in class_to_retrieve or nil
def retrieve_object(class_to_retrieve)
  (Rails.env == 'test') ? FactoryGirl.create(class_to_retrieve.name.underscore) : class_to_retrieve.first
rescue => error
  puts "ERROR retrieving #{class_to_retrieve.name}: #{error.message}\n"
  nil
end

# This method perfoms validations on string columns of an object
# @param obj [ActiveRecord::Base] an instance of subclass of ActiveRecord::Base to validate
# @param validators [Array] an array of ActiveModel::Validations::LengthValidator on string fields
# @param string_to_write [String] a string to use as value of the object's string fields to test
# @raise [StandardError] if the validations failed.
# @return [void]
def validate_obj(obj, validators, string_to_write)
  validators.each do |validator|
    options = validator.options

    # Test for exact length
    if options[:is].present?
      expectations = { exact: true, longer: false, shorter: false }
      validate_by_length(obj, validator, string_to_write, options[:is], expectations)
      next
    end

    # Test for maximum, minimum and range (which is converted to maximum and minimum)
    if options[:maximum].present?
      expectations = { exact: true, longer: false, shorter: true }
      validate_by_length(obj, validator, string_to_write, options[:maximum], expectations)
    end

    if options[:minimum].present?
      expectations = { exact: true, longer: true, shorter: false }
      validate_by_length(obj, validator, string_to_write, options[:minimum], expectations)
    end

  end
end #validate_obj


# This method validates a string column on an object using a LengthValidator
# @param obj [ActiveRecord::Base] an instance of subclass of ActiveRecord::Base to validate
# @param validator [ActiveModel::Validations::LengthValidator ] a length validator to run
# @param string_to_write [String] a string to use as value of the object's string fields to test
# @param limit [Integer] length value to be validated against. The validator validates exact, longer and shorter of the limit.
# @param expectations [Hash] a hash of expectations when validating the field. See code in 'validate_obj' for the hash structures and values
# @raise [StandardError] if the validations failed.
# @return [void]
def validate_by_length(obj, validator, string_to_write, limit, expectations)
  attribute = validator.attributes.first

  # Test exact value
  test_value = (string_to_write * limit)[0...limit]
  validator.validate_each(obj, attribute, test_value)
  raise 'Validate exact length failed.' unless (obj.errors.messages.empty? ==  expectations[:exact])
  obj.errors.clear

  # Test longer
  test_value = (string_to_write * limit)[0...(limit+1)]
  validator.validate_each(obj, attribute, test_value)
  raise 'Validate longer length failed.' unless (obj.errors.messages.empty? ==  expectations[:longer])
  obj.errors.clear

  # Test shorter
  test_value = (string_to_write * limit)[0...(limit-1)]
  validator.validate_each(obj, attribute, test_value)
  raise 'Validate shorter length failed.' unless (obj.errors.messages.empty? ==  expectations[:shorter])
  obj.errors.clear
end

# This is the main method that calls perform_test with different types of utf8mb4
# @raise [Error] if any of the tests failed.
# @return [void]
def run_tests
  raise 'This script cannot run in production!' if Rails.env == 'production'
  Rails.application.eager_load!

  # Test writing special variable length UTF8 characters
  perform_test("#{Time.now.to_f} Đây là một chuỗi tiếng Việt")

  # Test writing special 4-byte UTF8 characters: http://www.i18nguy.com/unicode/supplementary-test.html#utf8
  perform_test("#{Time.now.to_f} 𠜎𠜱𠝹𠱓𠱸𠲖𠳏𠳕𠴕𠵼𠵿𠸎")
end

run_tests
